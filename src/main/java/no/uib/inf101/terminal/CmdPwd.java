package no.uib.inf101.terminal;

public class CmdPwd implements Command{

    @Override
    public String run(String[] args) {
        return System.getProperty("user.dir");
    }

    @Override
    public String getName() {
        return "pwd";
    }
    
}
