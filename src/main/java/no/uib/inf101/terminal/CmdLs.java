package no.uib.inf101.terminal;

import java.io.File;

public class CmdLs implements Command{

    @Override
    public String run(String[] args) {
        File cwd = new File(System.getProperty("user.dir"));
        String s = "";
        for (File file : cwd.listFiles()) {
          s += file.getName();
          s += " ";
        }
        return s;
      }

    @Override
    public String getName() {
        return "ls";
    }
    
}
