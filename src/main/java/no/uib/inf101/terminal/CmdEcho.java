package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] words) {
        String result = "";
        for (String word : words) {
            result += word + " ";
        }
        return result;
    }

    @Override
    public String getName() {
       return "echo";
    }
    
}
