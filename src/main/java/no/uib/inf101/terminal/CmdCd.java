package no.uib.inf101.terminal;

public class CmdCd implements Command{

    @Override
    public String run(String[] args) {
        if (args.length == 0) {
            return "cd: missing argument";
        }
        String path = args[0];
        if (path.equals("..")) {
            String cwd = System.getProperty("user.dir");
            int lastSlash = cwd.lastIndexOf("/");
            String newCwd = cwd.substring(0, lastSlash);
            System.setProperty("user.dir", newCwd);
            return "";
        }
        if (path.equals(".")) {
            return "";
        }
        if (path.startsWith("/")) {
            System.setProperty("user.dir", path);
            return "";
        }
        String cwd = System.getProperty("user.dir");
        String newCwd = cwd + "/" + path;
        System.setProperty("user.dir", newCwd);
        return "";
    }

    @Override
    public String getName() {
        return "cd";
    }
    
}
